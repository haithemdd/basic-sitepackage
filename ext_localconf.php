<?php
defined('TYPO3_MODE') || die();

/*******************************
 * ADD default RTE Configuration
 *******************************/
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['basic_sitepackage'] = 'EXT:basic_sitepackage/Configuration/RTE/Default.yaml';

/*********
 * Page TS
 *********/
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:basic_sitepackage/Configuration/TsConfig/Page/All.tsconfig">');

/****************
 * Register Icons
 ****************/
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$icons = [];

foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'basicsitepackage-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:basic_sitepackage/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}