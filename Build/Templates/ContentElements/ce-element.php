<?php
$text = isset($text) ? $text :'';
?>

<style>
    .element {
        font-weight: 500;
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>

<div class="element">
    <?php if($text) { 
        echo $text;
    } ?>
</div>